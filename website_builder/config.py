#
# website_builder/config.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from dataclasses import dataclass
from typing import Optional, List

import yaml

from .input import InputDirectory
from .objects import Directory, Path, Proxy, Redirect, Rewrite, Site
from .output import OutputDirectory

__all__ = ["Config", "load_config"]


@dataclass
class Config:
    input: InputDirectory
    output: OutputDirectory
    sites: List[Site]
    default: Site
    pages: List[str]


def build_directory(path_data: dict) -> Directory:
    path = path_data["directory"]
    index = path_data.get("index")
    return Directory(path, index)


def build_proxy(path_data: dict) -> Proxy:
    url = path_data["proxy"]
    return Proxy(url)


def build_redirect(path_data: dict) -> Redirect:
    destination = path_data["redirect"]
    http_status = path_data.get("status", "permanent")
    escape = path_data.get("escape", True)

    if http_status == "permanent":
        http_status = "301"
    elif http_status == "temporary":
        http_status = "302"

    return Redirect(destination, http_status, escape)


def build_rewrite_rule(rewrite_data: dict) -> Rewrite:
    source = rewrite_data["source"]
    destination = rewrite_data["destination"]
    last = rewrite_data.get("last", False)
    return Rewrite(source, destination, last)


def parse_site_single(site_data: dict, domain: str) -> Site:
    root = parse_path("/", site_data)

    return Site(
        domain=domain,
        inherit=False,
        root=root,
        paths=[],
    )


def parse_path(url_path: str, path_data: dict):
    # Optional fields
    http_status = path_data.get("status")

    # Primary fields
    directory = None
    proxy = None
    redirect = None
    rewrite_rules = []

    if "directory" in path_data:
        directory = build_directory(path_data)
    elif "proxy" in path_data:
        proxy = build_proxy(path_data)
    elif "redirect" in path_data:
        redirect = build_redirect(path_data)

    for rewrite_data in path_data.get("rewrite-rules", []):
        rewrite_rules.append(build_rewrite_rule(rewrite_data))

    # Build and return
    return Path(
        url_path,
        directory,
        proxy,
        redirect,
        rewrite_rules,
        http_status,
    )


def parse_paths(base_site: Optional[Site], paths_data: dict, domain: str) -> Site:
    if base_site is None:
        inherit = False
        paths = []
    else:
        inherit = True
        paths = base_site.paths.copy()

    # Evaluate path objects for this site
    for path_data in paths_data:
        url_path = path_data["path"]
        paths.append(parse_path(url_path, path_data))

    return Site(
        domain=domain,
        inherit=inherit,
        root=None,
        paths=paths,
    )


def parse_site(base_site: Site, site_data: dict, domain: str = None) -> Site:
    has_single_field = any(field in site_data for field in ("proxy", "redirect"))

    if "paths" in site_data and has_single_field:
        raise ValueError(
            "You cannot specified a full-site configuration and also individual paths.",
        )

    if domain is None:
        domain = site_data["domain"]

    if has_single_field:
        return parse_site_single(site_data, domain)
    elif "paths" in site_data:
        return parse_paths(base_site, site_data["paths"], domain)
    else:
        return None


def load_config(path: str, output_dir: str):
    with open(path, encoding="utf-8") as file:
        data = yaml.safe_load(file)

    # Get base site
    base_site = parse_site(None, data["base"], "base")

    # Produce site list
    sites = []
    for site_data in data["sites"]:
        if site_data.get("inherit", True):
            base_site_used = base_site
        else:
            base_site_used = None

        site = parse_site(base_site_used, site_data)
        if site is not None:
            sites.append(site)

    # Get default site
    default = parse_site(base_site, data["default"], "default")

    # Get common pages
    pages = data["pages"]

    return Config(
        InputDirectory(),
        OutputDirectory(output_dir),
        sites,
        default,
        pages,
    )
