#
# website_builder/server.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import shutil

import nginxfmt

from .config import Config
from .input import InputDirectory
from .objects import Site
from .output import OutputDirectory

__all__ = ["build_nginx"]


def build_vhost(
    fmt: nginxfmt.Formatter,
    input: InputDirectory,
    output: OutputDirectory,
    site: Site,
    default: bool = False,
):
    template = input.jinja("nginx").get_template("vhost.j2")
    vhost_conf = template.render(
        default=default,
        domain=site.domain,
        root=site.root,
        paths=site.paths,
    )

    with output.create_file(f"{site.domain}.conf") as file:
        file.write(fmt.format_string(vhost_conf))


def build_vhosts(config: Config, output: OutputDirectory):
    formatter = nginxfmt.Formatter()
    output.create_directory("sites-enabled")
    output = output.create_directory("sites-available")

    for site in config.sites:
        build_vhost(formatter, config.input, output, site)

    build_vhost(formatter, config.input, output, config.default, True)


def build_nginx(config: Config):
    output = config.output.create_directory("nginx", create=False)

    # Copy static files
    shutil.copytree(config.input.get_path("nginx"), output.directory, symlinks=True)

    # Build virtual host configurations, per site
    build_vhosts(config, output)
