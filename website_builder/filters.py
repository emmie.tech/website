#
# website_builder/filters.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import re

NGINX_ESCAPE_REGEX = re.compile(r""".*["'\{\};\$\\ ]+.*""")


def nginx_escape(value, enabled=True):
    if enabled and NGINX_ESCAPE_REGEX.match(value):
        value = value.replace("'", r"\'")
        value = value.replace("$", r"\$")
        value = f"'{value}'"

    return value
