#
# website_builder/output.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import shutil
import os
from typing import IO


class OutputDirectory:
    __slots__ = ("output_path",)

    def __init__(self, output_path: str, create: bool = True):
        self.output_path = output_path

        # In uncommon cases where you want a directory object
        # but something else will create the directory itself.
        if create:
            os.makedirs(output_path, exist_ok=True)

    @property
    def directory(self):
        return self.output_path

    def get_path(self, *subpaths: str) -> str:
        return os.path.join(self.output_path, *subpaths)

    def create_file(self, subpath: str, binary: bool = False) -> IO:
        # pylint: disable=consider-using-with

        path = self.get_path(subpath)
        mode = "wb" if binary else "w"
        return open(path, mode, encoding="utf-8")

    def create_directory(self, subpath: str, create: bool = True) -> "OutputDirectory":
        return OutputDirectory(self.get_path(subpath), create=create)

    def clean(self):
        shutil.rmtree(self.output_path)
