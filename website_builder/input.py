#
# website_builder/input.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import os
import sys
from glob import iglob
from typing import Iterable, IO, Union

import jinja2
from jinja2 import Environment, FileSystemLoader

from .filters import nginx_escape

# For compatibility with pre-3.9
try:
    from functools import cache
except ImportError:
    from functools import lru_cache

    def cache(func):
        return lru_cache(maxsize=None)(func)


class InputDirectory:
    __slots__ = ("module_path", "input_path")

    def __init__(self):
        self.module_path = os.path.dirname(sys.argv[0])
        self.input_path = os.path.abspath(os.path.join(self.module_path, ".."))

    def get_path(self, *subpaths: str) -> str:
        return os.path.join(self.input_path, *subpaths)

    def glob(self, *pattern: str) -> Iterable[str]:
        return iglob(self.get_path(*pattern))

    def open_file(self, subpath: str, binary: bool = False) -> IO:
        # pylint: disable=consider-using-with

        path = self.get_path(subpath)
        mode = "rb" if binary else "r"
        return open(path, mode, encoding="utf-8")

    def read_file(self, subpath: str, binary: bool = False) -> Union[str, bytes]:
        with self.open_file(subpath, binary) as file:
            return file.read()

    @cache
    def jinja(self, subdirectory: str) -> jinja2.Environment:
        env = Environment(
            loader=FileSystemLoader(self.get_path("templates", subdirectory)),
            autoescape=lambda _: subdirectory == "html",
            extensions=["jinja2_time.TimeExtension"],
        )

        env.filters["nginx_escape"] = nginx_escape
        return env
