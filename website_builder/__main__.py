#
# website_builder/__main__.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import sys

from .config import load_config
from .html import build_html
from .server import build_nginx

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(f"Usage: {sys.argv[0]} <config> [output-dir]")
        sys.exit(1)

    if len(sys.argv) > 2:
        output_dir = sys.argv[2]
    else:
        output_dir = "dist"

    config = load_config(sys.argv[1], output_dir)
    config.output.clean()
    build_nginx(config)
    build_html(config)
    print("Built!")
    print(f"Outputted files in '{output_dir}'")
