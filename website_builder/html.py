#
# website_builder/html.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

import os
from typing import Optional

from minify_html import minify

from .config import Config
from .input import InputDirectory
from .objects import Site
from .output import OutputDirectory

__all__ = ["build_html"]


def build_html_page(
    template_name: str,
    page: str,
    input: InputDirectory,
    output: OutputDirectory,
    site: Optional[Site],
):
    template = input.jinja("html").get_template(template_name)
    page_html = template.render(
        template=template_name,
        page=page,
        site=site,
        domain=getattr(site, "domain", None),
    )

    with output.create_file(f"{page}.html") as file:
        minified_page_html = minify(
            page_html,
            do_not_minify_doctype=True,
            keep_spaces_between_attributes=True,
            minify_css=True,
            minify_js=True,
        )
        file.write(minified_page_html)


def build_html(config: Config):
    output = config.output.create_directory("html")

    # Build common pages
    for page in config.pages:
        template_name = f"{page}.j2"
        build_html_page(template_name, page, config.input, output, None)

    # Build site-specific pages
    for site in config.sites:
        site_output = output.create_directory(site.domain)

        for path in config.input.glob("templates", "html", site.domain, "*.j2"):
            filename = os.path.basename(path)
            page, _ = os.path.splitext(filename)
            template_name = os.path.join(site.domain, filename)
            build_html_page(template_name, page, config.input, site_output, site)

        # Symlink common pages
        if site.inherit:
            for page in config.pages:
                filename = f"{page}.html"
                source = os.path.join("..", filename)
                dest = site_output.get_path(filename)
                os.symlink(source, dest)
