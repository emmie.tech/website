#
# website_builder/objects.py
#
# website - My personal websites
# Copyright (c) 2021 Emmie Smith
#
# This software is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from dataclasses import dataclass

from typing import List, Optional


@dataclass
class Directory:
    destination: Optional[str]
    index: bool


@dataclass
class Proxy:
    url: str


@dataclass
class Redirect:
    destination: str
    http_status: str
    escape: bool


@dataclass
class Rewrite:
    source: str
    destination: str
    last: bool


@dataclass
class Path:
    path: str
    directory: Optional[Directory]
    proxy: Optional[Proxy]
    redirect: Optional[Redirect]
    rewrite_rules: List[Rewrite]
    http_status: Optional[str]


@dataclass
class Site:
    domain: str
    inherit: bool
    root: Optional[Path]
    paths: List[Path]
