## Personal Website

This is the code that powers my personal websites, [emmie.tech](https://emmie.tech/) and [ammonsmith.me](https://ammonsmith.me/).

Requires Python 3.7+. Available under the terms of the [MIT License](LICENSE.md).

### Execution

Run the following:

```
$ python3 -m website_builder config.yaml
```

The repository provides the configuration for my websites, but you can use any other file you wish. Generated artifacts are created in `dist/`.

If you wish to test your deployment locally, you can use the provided `Dockerfile` to build and run an image, or use `docker-compose`:

```
$ docker-compose up --build
```

Of course, changing the container names, image names, or ports if they conflict on your system. You can use `-d` instead of `-it` if you wish to have it run in the background.
