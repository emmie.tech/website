#
# Build
#

FROM python:3.9-slim as python

RUN mkdir /build
WORKDIR /build

# Install dependencies
COPY requirements.txt ./
RUN pip install -r requirements.txt

# Copy sources and build
COPY config.yaml ./
COPY nginx ./nginx
COPY static ./static
COPY templates ./templates
COPY website_builder ./website_builder
RUN python -m website_builder config.yaml

#
# Deployment (local)
#

FROM nginx:alpine

# Set up self-signed certificates
RUN apk add --no-cache openssl
RUN openssl req \
    -x509 \
    -newkey rsa:4096 \
    -sha256 \
    -days 3650 \
    -nodes \
    -keyout /etc/ssl/certs/self_signed.key \
    -out /etc/ssl/certs/self_signed.crt \
    -subj '/CN=localhost' \
    -addext 'subjectAltName=DNS:localhost,DNS:*.localhost'

# Copy sources
COPY --from=python /build/dist/html /var/www
COPY --from=python /build/dist/nginx /etc/nginx

# Enable all sites
RUN cd /etc/nginx/sites-enabled && \
    ln -s ../sites-available/* .

# Set up execution context
RUN adduser -SDH -h /var/www www-data
EXPOSE 80 443
WORKDIR /var/www
